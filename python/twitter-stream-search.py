#!/usr/bin/python

#-----------------------------------------------------------------------
# twitter-stream-format:
#  - ultra-real-time stream of twitter's public timeline.
#    does some fancy output formatting.
#-----------------------------------------------------------------------

from twitter import *
import re

search_term = "pluncfestival, plunc, festivalplunc, #plunc, #plunc2016, #captchat, #anaacolectivo, #tracingyou, #bengrosser, #eaudejardin, #phototropy, #portraitonthefly, #thevalueofart, #christasommerer, #laurentmignonneau, #transiconmorphosis, #emiliovavarella, #fitosegrera, #homeiswhereyourheartis, #nunocorreia, #artificialmesmerism, #thomasgrogan, #seabattle, #tiagororke, #estatica, #filipepais, #articaunderwater, #articacreativecomputing, #monicavlad, #antonioquirogawaldthaler, #speculativefutures, #jamesauger, #circadiem, #filipatomaz"



#-----------------------------------------------------------------------
# import a load of external features, for text display and date handling
# you will need the termcolor module:
#
# pip install termcolor
#-----------------------------------------------------------------------
from time import strftime
from textwrap import fill
from termcolor import colored
from email.utils import parsedate
import json

#-----------------------------------------------------------------------
# load our API credentials 
#-----------------------------------------------------------------------
config = {}
execfile("config.py", config)
ntweets = 0
#-----------------------------------------------------------------------
# create twitter API object
#-----------------------------------------------------------------------
auth = OAuth(config["access_key"], config["access_secret"], config["consumer_key"], config["consumer_secret"])
stream = TwitterStream(auth = auth, secure = True)

data = {"0": [{"text": ""}, {"author": "Plymewithblood"}]}

with open('tweets.json', 'w') as f:
			json.dump(data, f)

#-----------------------------------------------------------------------
# iterate over tweets matching this filter text
#-----------------------------------------------------------------------
tweet_iter = stream.statuses.filter(track = search_term)

pattern = re.compile("%s" % search_term, re.IGNORECASE)

for tweet in tweet_iter:
	
	# turn the date string into a date object that python can handle
	try:
		try:
			with open('tweets.json') as f:
				data = json.load(f)
		except:
			pass
		print 'old'
		print data
		print 'new'
		what = 'tweet' + str(ntweets)	 
		print what

		new_tweet = { str(ntweets) : [{ 'text' : tweet['text']}, {'author' : tweet['user']['screen_name'] } ] }
		#print new_tweet
		data.update(new_tweet)
		with open('tweets.json', 'w') as f:
			json.dump(data, f,  indent = 4, encoding = 'utf-8')


		#with open('tweets.json', 'a') as outfile:
		#	json.dump({ 'author': tweet['user']['screen_name'], 'text': tweet['text']}, outfile, indent = 4, sort_keys=True)
		#	outfile.write(',')
		#	outfile.write('\n]')

		timestamp = parsedate(tweet["created_at"])

		# now format this nicely into HH:MM:SS format
		timetext = strftime("%H:%M:%S", timestamp)

		# colour our tweet's time, user and text
		time_colored = colored(timetext, color = "white", attrs = [ "bold" ])
		user_colored = colored(tweet["user"]["screen_name"], "green")
		text_colored = tweet["text"]

		# replace each instance of our search terms with a highlighted version
		text_colored = pattern.sub(colored(search_term.upper(), "yellow"), text_colored)

		# add some indenting to each line and wrap the text nicely
		indent = " " * 11
		text_colored = fill(text_colored, 80, initial_indent = indent, subsequent_indent = indent)

		# now output our tweet
		print "(%s) @%s" % (time_colored, user_colored)
		print "%s" % (text_colored)
		ntweets = ntweets + 1
	except :
		pass
