#include "testApp.h"
ofTrueTypeFont font;

//--------------------------------------------------------------
void testApp::setup(){
    
    ofBackground(255);
    ofSetFrameRate(60);
    
    twitterClient.setDiskCache(true);
    twitterClient.setAutoLoadImages(false, true); // Loads images into memory as ofImage;
    
    string const CONSUMER_KEY = "sT6wTIXLsMDmRnQoD6M9d1Vzk";
    string const CONSUMER_SECRET = "U7HST2sNIOheMbPRMiIuOfrVBA0G33t8ogCCwSy7PR5ZHRG470";
    
    twitterClient.authorize(CONSUMER_KEY, CONSUMER_SECRET);
    
    actualTweet = 0;
    newTweetSearch = false;
    num_tweets = 0;
    lastTime = 0;
    currTime = 0;
    // search();
    monoLineTextInput.setup();
    monoLineTextInput.text = "";
    monoLineTextInput.bounds.x = 510;
    monoLineTextInput.bounds.y = 530;
    monoLineTextInput.bounds.height = 60;
    monoLineTextInput.bounds.width = 545;
    font.loadFont(OF_TTF_SERIF, 18);
    monoLineTextInput.setFont(font);
    
    
    _iswriting = false;
    ntests = 0;
    ofSetCircleResolution(250);
    ofSetVerticalSync(true);
    ofEnableAlphaBlending();
    monoLineTextInput.drawCursor = true;
    
    font12.loadFont("HelveticaNeue.ttf",12, true, true, 500);
    font22.loadFont("HelveticaNeue.ttf",22, true, true, 500);
    font20.loadFont("HelveticaNeue.ttf",20, true, true, 500);
    font14.loadFont("HelveticaNeue.ttf",14, true, true, 500);
    font26.loadFont("HelveticaNeue.ttf",15, true, true, 500);

}

//--------------------------------------------------------------
void testApp::update()
{
    
    /* if(ofGetElapsedTimef() - currTime > 30)
     {
     currTime = ofGetElapsedTimef();
     search();
     
     }*/
    
    // cout<<ofGetElapsedTimef() - lastTime<<endl;
    if(ofGetElapsedTimef() - lastTime > 20)
    {
        
        lastTime = ofGetElapsedTimef();
        //   twitterClient.postStatus("leave it running #johnnypersonalhashtag2 " + ofToString(ntests));
        ntests++;
        
    }
    
    if(newTweetSearch)
    {
        newTweetSearch = false;
        if(twitterClient.getTotalLoadedTweets() > num_tweets)
        {
            cout<<"NEW TWEETS TO ADD"<<endl;
            for(int i = 0; i < twitterClient.getTotalLoadedTweets() - num_tweets; i++)
            {
                tweet = twitterClient.getTweetByIndex(i);
                tweet_entry aux;
                aux.create(tweet.user.screen_name, tweet.text);
                if(num_tweets == 0)
                    tweets.push_back(aux);
                else
                    tweets.insert(tweets.begin(), aux);
            }
            num_tweets = twitterClient.getTotalLoadedTweets();
        }
    }
    
    if(!_iswriting && monoLineTextInput.getIsEditing())
    {
        _iswriting = true;
        open(1000);
    }
    
    if(_iswriting && !monoLineTextInput.getIsEditing())
    {
        _iswriting = false;
        if(monoLineTextInput.text != "")
            twitterClient.postStatus(monoLineTextInput.text);
        
        monoLineTextInput.clear();
        close(1000);
    }
    
    
    
}

//--------------------------------------------------------------
void testApp::draw()
{
    
    ofSetBackgroundColor(255, 255, 255);
    
    drawCover(0);
    /*ofRect(monoLineTextInput.bounds);
     
     ofNoFill();
     monoLineTextInput.draw();
     
     
     ofSetColor(255,0,0);
     
     
     ofCircle(ofGetMouseX(), ofGetMouseY(), 5);*/
    
}


void testApp::drawCover(int location)
{
    //BIG RECT
    ofPushMatrix();
    ofFill();
    ofSetColor(245, 248, 250);
    ofRect(0,0,ofGetWidth(), ofGetHeight());
    
    ofRect(0,430, ofGetWidth(), 70);//MEDIUM BAR
    
    ofSetColor(120,0,0);
    ofRectRounded(ofRectangle(100,290,240,240), 10);//BIG LOGO
    ofRect(435,490, 80,10);
    ofPopMatrix();
    
    
    ofPushMatrix();//tweets bar
    ofFill();
    ofSetColor(255,255,255);
    ofRectRounded(ofRectangle(440,610,620,55), 5);
    ofSetColor(204, 214, 221);
    ofNoFill();
    ofRectRounded(ofRectangle(440,610,620,55), 5);
    ofPopMatrix();
    
    //DRAW TEXT
    ofPushMatrix();
    ofSetColor(41, 47, 51);
    font22.drawString("PLUNC LISBOA", 100, 560);
    font20.drawString("TWEETS", 450, 645);
    ofSetColor(102,117, 127);
    font14.drawString("@festivalplunc", 100, 580);
    font12.drawString("TWEETS", 440, 450);
    font20.drawString(ofToString(tweets.size()), 460, 480);
    ofPopMatrix();
    
    for(int i = 0; i < tweets.size(); i++)
    {
        tweets[i].draw(i);
    }
    
    if(_iswriting || pop.isRunning())
    {
        ofFill();
        ofPushMatrix();
        ofSetColor(100,100,100,pop.update()*100);
        ofRect(0,0,ofGetWidth(), ofGetHeight());
        
        ofSetColor(245, 248, 250,255);
        ofRectRounded(ofRectangle(440, 525,620, 70 + pop.update()*75), 5);
        ofPopMatrix();
    }
    
    //text input draw
    ofPushMatrix();
    ofFill();
    ofSetColor(120,0,0);
    ofRectRounded(ofRectangle(100,290,240,240), 10);//BIG LOGO
    ofRectRounded(ofRectangle(445,530,60,60), 10);//SMALL LOGO
    ofRect(435,490, 80,10);
    ofPopMatrix();
    ofPushMatrix();
    ofNoFill();
    ofSetColor(204, 214, 221);
    ofSetLineWidth(1);
    ofRectRounded(ofRectangle(440, 525,620, 70 + pop.update()*75), 5);
    ofFill();
    ofSetColor(255,255,255);
    ofRect(monoLineTextInput.bounds.getTopLeft(), monoLineTextInput.bounds.getWidth(), monoLineTextInput.bounds.getHeight() +75*pop.update());
    ofSetColor(204, 214, 221);
    monoLineTextInput.text = wrapString(monoLineTextInput.text, 540);
    monoLineTextInput.draw();
    ofPopMatrix();
    
    ofPushMatrix();
    ofSetColor(255,255,255);
    ofRect(0,0,ofGetWidth(), 70);//UPPER BAR
    ofPopMatrix();
    
    ofFill();
    ofSetColor(255,0,0);
    ofCircle(ofGetMouseX(), ofGetMouseY(), 5);
    
}

void testApp::open(int duration)
{
    pop.setParameters(7,easinglinear,ofxTween::easeOut,0,1,duration,0);
    pop.start();
}


void testApp::close(int duration)
{
    pop.setParameters(7,easinglinear,ofxTween::easeOut,1,0,duration,0);
    pop.start();
}

//--------------------------------------------------------------
void testApp::keyPressed(int key){
    if(key == 's')
        search();
}

void testApp::search()
{
    cout<<"SEARCH"<<endl;
    ofxTwitterSearch search;
    search.query = "#sun";
    twitterClient.startSearch(search);
    newTweetSearch = true;
}


string testApp::wrapString(string text, int width) {
    
    string typeWrapped = "";
    string tempString = "";
    vector <string> words = ofSplitString(text, " ");
    
    for(int i=0; i<words.size(); i++)
    {
        string wrd = words[i];
        int stringwidth = font26.stringWidth(tempString + " " + wrd);
        if(stringwidth >= width)
        {
            tempString = wrd;
            typeWrapped += "\n";
        }
        else
            tempString += wrd + " ";
        if(typeWrapped != "")
            typeWrapped += wrd + " ";
    }
    
    return typeWrapped;
}

//--------------------------------------------------------------
void testApp::keyReleased(int key)
{
    
}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y ){
    
}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){
    
}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){
    
}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){
    
}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){
    
}

//--------------------------------------------------------------
void testApp::gotMessage(ofMessage msg){
    
}

//--------------------------------------------------------------
void testApp::dragEvent(ofDragInfo dragInfo){ 
    
}
