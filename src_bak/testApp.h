#pragma once

#include "ofMain.h"
#include "ofxTwitter.h"
#include "tweet.hpp"
#include "ofxTextInputField.h"
#include "ofxTween.h"

class testApp : public ofBaseApp
{
    
public:
    
    void setup();
    void update();
    void draw();
    void search();
    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y );
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void windowResized(int w, int h);
    void dragEvent(ofDragInfo dragInfo);
    void gotMessage(ofMessage msg);
    
    void drawCover(int location); //0 Lisboa  1 Almada
    void open(int duration);
    void close(int duration);
    string wrapString(string text, int width);
    ofxTwitter twitterClient;
    ofxTwitterTweet tweet;
    int actualTweet;
    bool reloadTweet;
    
    ofImage userImage;
    ofxTween pop;
    ofxEasingLinear 	easinglinear;
    bool newTweetSearch;
    vector <tweet_entry> tweets;
    int num_tweets;
    
    float lastTime;
    float currTime;
    
    ofxTextInputField monoLineTextInput;
    bool _iswriting;
    int ntests;
    
    ofTrueTypeFont font22, font14, font20, font12, font26;
    
};
